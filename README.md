# Docker Image for Bedrock Dedicated Server (Alpha)

## Getting Started

### 1. Building image
```
docker build -t bedrock:latest .
```

### 2. Running docker container
```
docker run -d -it \ 
  --name bedrock \
  -p 19132:19132/udp \
  -v ./config:/app/config \
  -v ./worlds:/app/worlds \
  bedrock:latest
```

## Running with compose

### With a name
```
NAME=bedrock docker-compose up -d
```

### With default name
```
docker-compose up -d
```

### Stopping
```
docker-compose down
```
